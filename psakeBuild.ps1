properties {
    $script:module_path = 'C:\Program Files\WindowsPowerShell\Modules'
    $script:scripts = New-Object -TypeName System.Collections.ArrayList
    $script:signed_scripts = New-Object -TypeName System.Collections.ArrayList
    $script:tokens = $PSScriptRoot.Split("`\")
    $script:module_root = $tokens[$tokens.length -1]
    $script:scratch_folder = "$env:TEMP\$module_root"
    $script:certificate = Resolve-path -path "C:\Code\certificate.txt"
    $script:current_user_all_hosts = "$(split-path($PROFILE.CurrentUserAllHosts))\Modules"

    $ConfigFile = "$PSSCriptRoot\$script:module_root\Private\new_hire_config.json"
    $filestream = (Get-Content -Path $ConfigFile -Raw)
    $script:NewHireConfigurationVariables = [PSCustomObject](ConvertFrom-Json -InputObject $filestream)
    $ValidateCompany = ($script:NewHireConfigurationVariables.company).Split(",")
    $global:ValidCompany = @{}
    foreach ($temp in $ValidateCompany)
    {
        $tmp = $temp.Split(":")
        $global:ValidCompany[$tmp[0]] = $tmp[1]
    }

    Get-ChildItem -Path $PSScriptRoot\$script:module_root\Public  -Filter *-*.ps1  | ForEach-Object {[void]$scripts.Add($_.FullName) }
    Get-ChildItem -Path $PSScriptRoot\$script:module_root\Private  -Filter *-*.ps1  | ForEach-Object {[void]$scripts.Add($_.FullName) }

    Write-Verbose -Message "*********************************"
    Write-Verbose -Message "*********PSake Build*************"
    Write-Verbose -Message "Module root: $script:module_root"
    Write-Verbose -Message "Scratch folder: $script:scratch_folder"
    Write-Verbose -Message "Script root: $PSScriptRoot"
    Write-Verbose -Message "Certificate: $script:certificate"
    Write-Verbose -Message "Config file: $ConfigFile"
    Write-Verbose -Message "New Hire config variables: $script:NewHireConfigurationVariables"
    Write-Verbose -Message "$ValidateCompany"
    Write-Verbose -Message "*********************************"

}

task default -depends Analyze, Test

task Analyze {
    $saResults = New-Object -TypeName System.Collections.ArrayList
    foreach ($script in $script:scripts)
    {
        $saResults = Invoke-ScriptAnalyzer -Path $script -Severity @('Error'<#,'Warning'#>) -Recurse -Verbose:$false
        if ($saResults) {
            $saResults | Format-Table
            Write-Error -Message 'One or more Script Analyzer errors/warnings where found. Build cannot continue!'
        }
    }
}

task Test {

    Get-PSSession | Remove-PSSession

    Remove-Module EveAdmin -Force -ErrorAction SilentlyContinue | Out-Null
    Remove-Module Office365 -Force -ErrorAction SilentlyContinue | Out-Null

    Import-module "C:\code\Office365\Office365" -Global -Verbose
    Import-module "C:\code\EveAdmin\EveAdmin" -Global -Verbose

    $testResults = Invoke-Pester -Path $PSScriptRoot -PassThru
    if ($testResults.FailedCount -gt 0) {
        $testResults | Format-List
        Write-Error -Message 'One or more Pester tests failed. Build cannot continue!'
    }
}

task Sign {
    <#
    get certificate
    create workspace
    copy module into workspace.
    sign scripts and 
    #>


    Write-Verbose "[Verbose]`tGrabbing certificate path."
    $securecontents = $(Get-Content -Path $script:certificate | ConvertTo-SecureString)
    $creds = New-Object System.Management.Automation.PSCredential("username", $securecontents)
    $certificate = get-childitem -Path $creds.GetNetworkCredential().password 
    
    if (Test-Path -Path $script:scratch_folder)
    {
        Remove-Item -Path $script:scratch_folder -Recurse -Force
    }
    Write-Verbose "[Verbose]`t$certificate"
    $local:scratch = New-Item -ItemType "directory" -Name $script:module_root  -Path $env:TEMP
    Write-Verbose "[Verbose]`t$($local:scratch.GetType())"
    if ( $local:scratch.GetType() -ne [System.IO.DirectoryInfo] )
    {
        Write-Error -Message "One or more Script Analyzer errors/warnings where found. Build cannot continue!`n$($error[0])"
    }


    Copy-Item -Path $PSScriptRoot\$script:module_root\* -Destination $local:scratch.FullName -Recurse -Exclude *.json

    
    Get-ChildItem -Path $local:scratch.FullName  -Filter *.ps?1  | ForEach-Object {[void]$signed_scripts.Add($_.FullName) }
    Get-ChildItem -Path $local:scratch.FullName  -Filter *-*.ps1 -Recurse  | ForEach-Object {[void]$signed_scripts.Add($_.FullName) }

    $saResults = New-Object -TypeName System.Collections.ArrayList
    foreach( $signed_script in $signed_scripts)
    {
        Write-Verbose "[Verbose]`tSigning $($signed_script)"
        $saResults = Set-AuthenticodeSignature -Certificate $certificate -FilePath $signed_script
        if ($saResults.Status -ne "Valid") {
            $saResults | Format-Table
            Write-Error -Message 'One or more Script Analyzer errors/warnings where found. Build cannot continue!'
        }
    }

    $catalog = New-FileCatalog -Path $local:scratch.FullName -CatalogFilePath "$($local:scratch.FullName)\$module_root.cat"

    $saResults = New-Object -TypeName System.Collections.ArrayList
    Write-Verbose "[Verbose]`tSigning $($catalog.FullName)"
    $saResults = Set-AuthenticodeSignature -Certificate $certificate -FilePath $catalog.FullName
    if ($saResults.Status -ne "Valid") {
        $saResults | Format-Table
        Write-Error -Message 'One or more Script Analyzer errors/warnings where found. Build cannot continue!'
    }
}

task Deploy -depends Analyze, Test, Sign {
    Remove-Module EveAdmin -Force | Out-Null
    Remove-Module Office365 -Force | Out-Null    
    
    $module_profile_path = "$(split-path($PROFILE.CurrentUserAllHosts))\Modules\$script:module_root"
    Write-Output "Removing Module Folder: `t$module_profile_path"

    if (Test-Path -Path $module_profile_path)
    {
        Remove-Item -Path $module_profile_path -Recurse -Force
    }

    $from = $script:scratch_folder
    $to = $module_profile_path

    Copy-Item -path $from -Destination $to -Recurse
    Remove-Item -Path $from -Recurse -Force

    write-output "We have moved the files: `nFrom:`t`t$from `nTo:`t`t$to"

    Copy-Item -Path "$PSScriptRoot\$script:module_root\Private\new_hire_config.json" -Destination "$to\Private\new_hire_config.json"
}



task Publish -depends Analyze, Test, Sign, Deploy { 
    if ($Env:PSModulePath -notmatch [Regex]::Escape($script:current_user_all_hosts))
    {
        Write-Output "Module Path: $($script:current_user_all_hosts)"
        #$Env:PSModulePath = "C:\Program Files\WindowsPowerShell\Modules;C:\Windows\System32\WindowsPowerShell\v1.0\Modules;C:\Users\byron\AppData\Local\Temp\2144886163" # -join($Env:PSModulePath,";",$script:module_profile_path)
    }

    Write-Output "Importing Office365 module..."
    Import-module Office365 -Verbose
    Import-module $script:current_user_all_hosts\EveAdmin -Verbose

    Invoke-PSDeploy -Path "$PSScriptRoot\$script:module_root.psdeploy.ps1" -Force -Verbose:$VerbosePreference
}