BeforeAll {
    Write-Host "Testing New-ADDSUserObject" -ForegroundColor Red -BackgroundColor Yellow
}

Describe "New-ADDSUserObject" {
    InModuleScope Eveadmin {        
        Context "" {
            BeforeEach  {
            }

            It "" {
                
                $UserObject = @{    
                    Company = $Company
                    GivenName = $GivenName
                    Surname = $Surname
                    Title = $Title
                    DisplayName = $AzureUserAccount.DisplayName
                    Password = $SecurePassword
                    Department = $Department
                    ManagerMail = $ManagerUPN
                    MailNickName = $AzureUserAccount.MailNickName
                    EmailAddress = $AzureUserAccount.UserPrincipalName
                    OU = $OU[$Company]
                }

                New-EverestEmployee @params -Verbose
                Assert-VerifiableMock
            }
        }
    }
}

AfterAll {
    Get-PSSession | Remove-PSSession
}