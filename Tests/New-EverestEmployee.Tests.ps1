BeforeAll {
    Write-Host "Testing New-EverestEmployee" -ForegroundColor Red -BackgroundColor Yellow

    get-command -Module Office365 -Verbose | Out-String | Write-Host
    get-command -Module EveAdmin -Verbose | Out-String | Write-Host

    Get-Module EveAdmin, Office365 | Out-String | Write-Host
    
    mock -ModuleName EveAdmin New-Employee {        
        return @{
            Title = "Manager"
            DisplayName ="Bob Jones"
            Department ="Technology Services"
            Manager ="Bob Jones"
            MailNickName = "tuser"
            UserPrincipalName = "tuser"
            GivenName = "Bob"
            ObjectId = "kjakjfdhlfoiuysaflkhbnsefl,kiyawdf"
        } 
    }
    mock -ModuleName EveAdmin New-ADDSUserObject { return $true}
    mock -ModuleName EveAdmin Add-ADDSGroupMember { return $true}
    mock -ModuleName EveAdmin Get-ADDSUser { return $getadreturnvalue }     
    mock Get-AzureADUserDetail {
        return @{
            Title = "Manager"
            DisplayName ="Bob Jones"
            Department ="Technology Services"
            Manager ="Bob Jones"
            MailNickName = "tuser"
            UserPrincipalName = "tuser"
            GivenName = "Bob"
            ObjectId = "kjakjfdhlfoiuysaflkhbnsefl,kiyawdf"
        }
    }
    mock Get-AzureADUserManager {
        return @{
            GivenName = "Bob"
        }
    }
}

InModuleScope EveAdmin {
    Describe "New-EverestEmployee" {
        Context "We are creating accounts for the various FYE employee requests" {
            BeforeAll{ 
                mock Write-MyLog { }
                mock Add-ADGroupMember {}
            }
            It "  Creates an AZAD and AD Account for an fye employee" {
                Write-Host "Test 1" -ForegroundColor Red -BackgroundColor DarkMagenta
                $params = @{
                    respondant = "byron@everesttoys.com"
                    company = "FYE"
                    givenname = "Jennifer"
                    surname = "Corey"
                    title = "Warehouse Supervisor"
                    department = "Warehouse"
                    ManagerUPN = "byron@everesttoys.com"
                    groupmemberships = ""
                    computerrequired = ""
                    email ="No"
                    intune = "No"
                    vpn = "No"
                    phonerequired = "No"
                    test = $true
                }

                mock -ModuleName EveAdmin Send-MailMessage { return "jcorey" } 
                mock -ModuleName EveAdmin Get-DisplayName { return "Jennifer Corey"}
                mock -ModuleName EveAdmin New-SCNUsername {return "jcsdf"}
                mock -ModuleName EveAdmin New-Username { return "jcorey" }   

                New-EverestEmployee @params -Verbose
                Assert-VerifiableMock
            }

            It "  Creates an AD Account" {
                Write-Host "Test 2" -ForegroundColor Red -BackgroundColor DarkMagenta
                $params = @{
                    respondant = "byron@everesttoys.com"
                    company = "FYE"
                    givenname = "For"
                    surname = "Your Entertainment"
                    title = "FYE Buyer/Analyst"
                    username = "fye"
                    department = "Procurement"
                    ManagerUPN = "byron@everesttoys.com"
                    groupmemberships = ""
                    computerrequired = ""
                    email ="no"
                    intune = "No"
                    vpn = "No"
                    phonerequired = "No"
                    test = $false
                }          

                mock -ModuleName EveAdmin Send-MailMessage { return "jcorey" } 
                mock -ModuleName EveAdmin Get-DisplayName { return "For Your Entertainment"}
                mock -ModuleName EveAdmin New-SCNUsername {return "fesdf"}
                mock -ModuleName EveAdmin New-Username { return "fyentertainment" }   

                New-EverestEmployee @params -Verbose
                Assert-VerifiableMock
            }

            It "  Creates an AZAD and AD Account" {
                Write-Host "Test 3" -ForegroundColor Red -BackgroundColor DarkMagenta
                $params = @{
                    respondant = "byron@everesttoys.com"
                    company = "Everest Toys"
                    givenname = "Test"
                    surname = "User"
                    title = "Test User"
                    department = "Technology Services"
                    ManagerUPN = "byron@everesttoys.com"
                    groupmemberships = ""
                    Office365Subscription = "E3"
                    computerrequired = ""
                    email ="yes"
                    intune = "No"
                    vpn = "No"
                    phonerequired = "No"
                    test = $true
                }

                mock -ModuleName EveAdmin Send-MailMessage { return "tuser" } 
                mock -ModuleName EveAdmin Get-DisplayName { return "Test User"}
                mock -ModuleName EveAdmin New-SCNUsername {return "tusdf"}
                mock -ModuleName EveAdmin New-Username { return "tuser" }   

                New-EverestEmployee @params -Verbose
                Assert-VerifiableMock
            }


            It "Creates an AD SCN Account" {
                Write-Host "Test 4" -ForegroundColor Red -BackgroundColor DarkMagenta
                $params = @{
                    respondant = "byron@everesttoys.com"
                    company = "FYE"
                    givenname = "For"
                    surname = "Your Entertainment"
                    title = "Warehouse Associate"
                    username = "fye"
                    department = "Warehouse"
                    ManagerUPN = "byron@everesttoys.com"
                    groupmemberships = ""
                    Office365Subscription = "E3"
                    computerrequired = ""
                    email ="no"
                    intune = "No"
                    vpn = "No"
                    phonerequired = "No"
                    test = $true
                }

                mock -ModuleName EveAdmin Send-MailMessage { return "jcorey" } 
                mock -ModuleName EveAdmin Get-DisplayName { return "For Your Entertainment"}
                mock -ModuleName EveAdmin New-SCNUsername {return "fesdf"}
                mock -ModuleName EveAdmin New-Username { return "fyentertainment" }               

                New-EverestEmployee @params -Verbose
                Assert-VerifiableMock
            }
        }
    }
}

AfterAll {
    Get-PSSession | Remove-PSSession
}