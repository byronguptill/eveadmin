BeforeAll {
    Write-Host "Testing Get-DisplayName" -ForegroundColor Red -BackgroundColor Yellow
}
Describe "Get-DisplayName" {
    InModuleScope Eveadmin {
        Context "Generate the Display Name" {
            It "Should return new user." {
                $GivenName = "Byron"
                $Surname = "Guptill"
                $Title = "Technology Manager"
                $Object = Get-DisplayName -GivenName $GivenName -Surname $Surname -Title $Title
                $Object | Should -Be "Byron Guptill (Technology Manager)"
            }
        }
    }
}
