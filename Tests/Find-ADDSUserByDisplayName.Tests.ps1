BeforeAll {
    Write-Host "Testing Find-ADDSUserByDisplayName" -ForegroundColor Red -BackgroundColor Yellow
}
Describe "Find-ADDSUserByDisplayName" {
    InModuleScope EveAdmin {
        Context "Checks ADDS for the existance of an object by using the Display Name" {
            It "Will find an object that represent Byron Guptill" {
                $Object = Find-ADDSUserByDisplayName -DisplayName "Byron Guptill"
                $Object | Should -Not -Be $null 
            }

            It "Will not find an object as there is no match"{
                $Object = Find-ADDSUserByDisplayName -DisplayName "Black Sabbath"
                $Object | Should -Be $null 
            }
        }
    }
}