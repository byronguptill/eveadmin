BeforeAll {
    Write-Host "Testing Convert-TextToImage" -ForegroundColor Red -BackgroundColor Yellow
}

Describe "Convert-TextToImage" {
    InModuleScope EveAdmin {
        Context "Create Text to Image " {
            It "Will find an object that represent Byron Guptill" {
                $Object = Convert-TextToImage "Byron Guptill"
                $Object | Should -Not -Be $null 
            }
        }
    }
}