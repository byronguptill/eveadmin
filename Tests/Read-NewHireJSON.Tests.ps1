BeforeAll {
    Write-Host "Testing Read-NewHireJSON" -ForegroundColor Red -BackgroundColor Yellow
}

Describe "Read-NewHireJSON" {
    InModuleScope Eveadmin {

        BeforeEach {
            $here = $(Split-Path $PSScriptRoot -Parent)
            $test_path = "$PSScriptRoot\read"
        }
        
        Context "There are no JSON files in the specified path" {
            It "Will return `$null as no files found" {
                $Objects = Read-NewHireJSON -Path $here
                $Objects.Count | Should -Be 0
            }
        }
        
        Context "There are JSON files in the specified path" {
            It "Will find one object" {
                $Objects = Read-NewHireJSON -Path $test_path 
                $Objects.Count | Should -Be 1
            }
            It "will match this." {
                $Objects = Read-NewHireJSON -Path $test_path 

                $compareTo = [PSCustomObject]@{
                    respondant="anonymous"
                    company="Alex Global Products"
                    email="No"
                    givenname="Bazinga"
                    surname="Test"
                    title="Senior Account Manager"
                    department="Sales"
                    managername="byron@everesttoys.com"
                    groupmemberships= "Alex Global Products|Procurement"
                    computerrequired=""
                    intune="No"
                    vpn="Yes"
                    phonerequired="No"
                    Office365Subscription = "E3"
                }            

                $compare_us = @{
                    ReferenceObject = $compareTo.PSObject.Properties
                    DifferenceObject = $Objects.item(0).Object.PSObject.Properties
                } 
                $compare = Compare-Object @compare_us -IncludeEqual
                $compare.Sideindicator | Should -Be "=="
            }
            It "will not match this." {
                $groups = [PSCustomObject]@{
                    group1 = "salesteam"
                    group2 = "orders"
                }
                $compareTo = [PSCustomObject]@{
                    company="SunriseRecords"
                    givenname="Led"
                    surname="Zeppelin"
                    title="Music Director"
                    username="Led" 
                    department="Purchasing"
                    managername="byron@everesttoys.com"
                    groupmemberships= $groups
                    computerrequired="Yes"
                    phonerequired="Yes"
                }
                $Objects = Read-NewHireJSON -Path $test_path 
                $compare_us = @{
                    ReferenceObject = $compareTo.PSObject.Properties
                    DifferenceObject = $Objects.item(0).Object.PSObject.Properties
                } 
                $compare = Compare-Object @compare_us -IncludeEqual
                $compare.Sideindicator | Should -Not -Be "=="
            }        
        }
    }
}

AfterAll {
    Get-PSSession | Remove-PSSession
}