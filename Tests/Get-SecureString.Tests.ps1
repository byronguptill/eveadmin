BeforeAll {
    Write-Host "Testing Get-SecureString" -ForegroundColor Red -BackgroundColor Yellow
}

Describe "Get-SecureString" {
    InModuleScope Eveadmin {
        Context "Create a secure String" {
            It "Should return new user." {
                $SecurePassword = new-object SecureString
                $Object = Get-SecureString
                $Object.GetType() | Should -Be $SecurePassword.GetType()
            }
        }
    }
}

AfterAll {
    Get-PSSession | Remove-PSSession
}