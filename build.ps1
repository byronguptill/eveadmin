[cmdletbinding()]
param(
    [string[]]$Task = 'default'
)



<#
$url = "https://www.nuget.org/api/v2/package/System.Text.Json/System.Text.Json.4.7.1"
$output = "$PSScriptRoot\system.text.json.4.7.1.nuget"

(New-Object System.Net.WebClient).DownloadFile($url, $output)


$url = https://www.nuget.org/api/v2/package/NJsonSchema/10.1.12
$output = "$PSScriptRoot\NJsonSchema.10.1.12.nuget"
#>

if (!(Get-Module -Name Pester -ListAvailable)) { Install-Module -Name Pester -Scope CurrentUser }
if (!(Get-Module -Name psake -ListAvailable)) { Install-Module -Name psake -Scope CurrentUser }
if (!(Get-Module -Name PSDeploy -ListAvailable)) { Install-Module -Name PSDeploy -Scope CurrentUser }

Invoke-psake -buildFile "$PSScriptRoot\psakeBuild.ps1" -taskList $Task -Verbose:$VerbosePreference