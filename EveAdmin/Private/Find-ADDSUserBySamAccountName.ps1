function Find-ADDSUserBySamAccountName {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true)]    
        [String]
        $samAccountName,
        [Parameter(Mandatory=$false)]
        [String]
        $Server,
        [Parameter(Mandatory=$false)]
        [PSCredential]
        $Credential
    )
    
    begin {
    }
    
    process {

        if ($PSBoundParameters.ContainsKey("Server") -and $PSBoundParameters.ContainsKey("Credential"))
        {
            $UserObject = get-aduser -Identity $SamAccountName -Server $Server -Credential $Credential
        } else {
            $UserObject = get-aduser -Identity $SamAccountName
        }

    }
    
    end {
        $UserObject
    }
}