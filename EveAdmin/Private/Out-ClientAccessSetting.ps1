function Out-ClientAccessSetting {
    [CmdletBinding()]
    param (
        $UserPrincipalName,
        $FilePath,
        $Intune
    )
    
    begin {
        Write-Verbose "$($MyInvocation.MyCommand) Set Client Access rules: $Intune"
        $DateString = $((get-date).ToLocalTime()).ToString("yyyyMMddHHmmss")
        $NewName = "$UserPrincipalName$DateString$(Get-Random)"        
    }
    
    process {
        try
        {

            if ($Intune -eq "Yes")
            {
                Out-File -FilePath "$FilePath\$NewName.json" `
                        -InputObject "{`n`t`"AccessSetting`" : `"Default`",`n`t`"upn`" : `"$UserPrincipalName`"`n}"
            }
            else
            {
                Out-File -FilePath "$FilePath\$NewName.json" `
                        -InputObject "{`n`t`"AccessSetting`" : `"DesktopOWA`",`n`t`"upn`" : `"$UserPrincipalName`"`n}"
            }
        }
        catch 
        {
            Write-Error $Error
        }
    }
    
    end {
        
    }
}