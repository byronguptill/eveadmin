function Get-DisplayName {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true)]
        [String]
        $GivenName,
        [Parameter(Mandatory=$true)]
        [String]
        $Surname,
        [Parameter(Mandatory=$true)]
        [String]
        $Title

    )
    
    begin {
        Write-Verbose "$($MyInvocation.MyCommand)`t`tSetting display name."
        $NewHireDisplayName = -join($GivenName, " ", $Surname)
    }
    
    process {

        #check AD for the existance of the default display name 
        #if not exists 
            #use it.
        #else
            #display name (title)

        while ($null -ne (Find-ADDSUserByDisplayName -DisplayName $NewHireDisplayName) )
        {
            switch ($NewHireDisplayName) 
            {
                ( -join($GivenName, " ", $Surname)) 
                {
                    Write-Verbose "$($MyInvocation.MyCommand)`t`t`"$NewHireDisplayName`" is already in use."
                    $NewHireDisplayName = -join($GivenName, " ", $Surname, " (", $Title,")")
                }
                ( -join($GivenName, " ", $Surname, " (", $Title,")")) 
                { 
                    Write-Verbose "$($MyInvocation.MyCommand)`t`t`"$NewHireDisplayName`" is already in use. Throw."
                    throw "Unable to generate unique Display Name for person: $GivenName $Surname"   
                }
            }
        }
    }
    
    end {
        $NewHireDisplayName
    }
}