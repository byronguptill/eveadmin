function Get-SecureString {
    [CmdletBinding()]
    param (
        
    )
    
    begin {
        Write-Verbose "$($MyInvocation.MyCommand)`t`tGenerating password."
    }
    
    process {
        $SecurePassword = new-object securestring
        1..14 | ForEach-Object {$SecurePassword.AppendChar([char][System.Web.Security.Membership]::GeneratePassword(1,0))}
    }
    
    end {
        $SecurePassword
    }
}