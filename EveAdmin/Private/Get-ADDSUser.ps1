function Get-ADDSUser {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$false)]
        $Identity,
        [Parameter(Mandatory=$false)]
        $Filter
    )
    
    begin {
        
    }
    
    process {
        if ($PSBoundParameters['Filter'])
        {
            $ReturnVal = Get-ADUser -Filter $Filter
        }
        if ($PSBoundParameters['Identity'])
        {
            $ReturnVal = Get-ADUser -Identity $Identity
        }        
    }
    
    end {
        $ReturnVal
    }
}
<#
$user = Get-ADDSUser -Identity $SamAccountName
$user = Get-ADDSUser -Filter {mail -eq $ManagerUPN}
#>