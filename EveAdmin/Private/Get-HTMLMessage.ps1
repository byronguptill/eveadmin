function Get-HTMLMessage {
    [CmdletBinding()]
    param (
        $Username,
        $ManagerName,
        $NewHireDsiplayName,
        $NewHireEmailAddress,
        [Parameter(Mandatory=$true)]
        $setupmode
    )
    
    begin {
        switch ($setupmode)
        {
            1 {
                $body = @"
<html>
<body style="font-family:calibri"> 
Dear $ManagerName,<br><br>

Your new hire, $NewHireDsiplayName, has been setup.<br><br>

Windows username: $Username<br>

Password: <img src="cid:image1"><br>

</body>
</html>
"@
                break;
            }  
            2 {
                $body = @"
<html>
<body style="font-family:calibri"> 
Dear $ManagerName,<br><br>

Your new hire, $NewHireDsiplayName, has been setup.<br><br>

Windows username: $Username<br>

Password: <img src="cid:image1"><br>

</body>
</html>
"@
                break;
            }  
            default { 
                $body = @"
<html>
<body style="font-family:calibri"> 
Dear $ManagerName,<br><br>

Your new hire, $NewHireDsiplayName, has been setup.<br><br>

Windows username: $Username<br>

Email address: $NewHireEmailAddress<br>

Temporary password: <img src="cid:image1"><br>

<h3>Activate email</h3>
<ul>
<li>Visit the Microsoft Office 365 portal - <a href"https://www.office.com/">https://www.office.com/</a></li>
<li>Click "Sign in".</li>
<li>Enter your email address as specified above.</li>
<li>Click "Next".</li>
<li>Enter your password as specified above.</li>
<li>Click "Sign in".</li>
<li>Enter your password as specified above in the "Current password field".</li>
<li>Enter a new strong password in the "New password" field.</li>
<li>Confirm your new password by re-typing it in the "Confirm password" field.</li>
<li>Click "Sign in".</li>
</ul>

<h3>Open Microsoft Outlook</h3>
<ul>
<li>On your Desktop, double click on the Outlook icon to open Microsoft Outlook.</li>
<li>On the first run, it will prompt you to enter your email address.  Ensure that it matches the address specified above.</li>
<li>Click "Connect".</li>
<li>Outlook will prompt for your password:</li>
<li>You must correct the username from username@everest.local to <a href="mailto:$NewHireEmailAddress">$NewHireEmailAddress</a>.</li>
<li>Enter your password.</li>
<li>Select "Remember my credentials".</li>
<li>Click "OK".</li>
<li>Outlook will setup your email account.</li>
</ul>
</body>
</html>
"@
                break;
            }
        }
    }
    
    process {
        
    }
    
    end {
        $body
    }
}