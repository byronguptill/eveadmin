function Add-ADDSGroupMember {
    [CmdletBinding()]
    param (
        [String]
        $Identity,
        [String]
        $Members,
        [Parameter(Mandatory=$false)]
        [String]
        $Server,
        [Parameter(Mandatory=$false)]
        [PSCredential]
        $Credential
    )
    
    begin {
    }
    
    process {
        if ($PSBoundParameters.ContainsKey("Server") -and $PSBoundParameters.ContainsKey("Credential"))
        {
            Add-ADGroupMember -Identity $Identity -Members $Members -Server $Server -Credential $Credential
        } else {
            Add-ADGroupMember -Identity $Identity -Members $Members
        }
    }
    
    end {
    }
}

