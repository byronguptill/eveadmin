function Write-MyLog {
    [CmdletBinding()]
    param (

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]$Message

    )
    begin {
    }
    process {
        Write-Verbose $Message
        Write-EventLog -LogName Application -Source EveAdmin -EventId 65001 -EntryType Information -Message $Message

    }
    end {

    }
}