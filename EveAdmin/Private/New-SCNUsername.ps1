function New-SCNUsername {
    [CmdletBinding()]
    param (

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]$GivenName,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]$Surname

    )
    begin {
    }
    process {
        while ($count -lt 15)
        {
            $random = (97..122 | Get-Random -Count 3 | ForEach-Object { ([char]$_) }) -join ""
            $Username =  -join($GivenName.Substring(0,1), $Surname.Substring(0,1),$random)
            Write-Verbose "$($MyInvocation.MyCommand) User created in on-prem AD: $user"
            try 
            {
                get-aduser $Username | Out-Null
            }
            catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException]
            {
                break;
            }            
            $count++
        }
    }
    end {
        , [String]$Username.ToLower()
    }
}