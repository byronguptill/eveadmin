function Find-ADDSUserByDisplayName {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullorEmpty()]
        [String]
        $DisplayName,
        [Parameter(Mandatory=$false)]
        [String]
        $Server,
        [Parameter(Mandatory=$false)]
        [PSCredential]
        $Credential
    )
    
    begin {
    }
    
    process {
        if ($PSBoundParameters.ContainsKey("Server") -and $PSBoundParameters.ContainsKey("Credential"))
        {
            $UserObject = get-aduser -Filter {DisplayName -eq $DisplayName} -Server $Server -Credential $Credential
        } else {
            $UserObject = get-aduser -Filter {DisplayName -eq $DisplayName} 
        }
    }
    
    end {
        $UserObject
    }
}