﻿#Requires -Version 5.1


$ConfigFile = "$PSSCriptRoot\Private\new_hire_config.json"

if (-not (Test-Path -Path $ConfigFile)){
    Write-Verbose -Message "Testing for the existance of the config file."
    throw "File not found: $ConfigFile"
}

Add-Type -AssemblyName PresentationFramework

[void] [Reflection.Assembly]::LoadFrom("$PSSCriptRoot\Assemblies\system.text.json\4.7.0\lib\net461\System.Text.Json.dll")
[void] [Reflection.Assembly]::LoadFrom("$PSSCriptRoot\Assemblies\njsonschema.10.1.5\lib\net45\NJsonSchema.dll")

#[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing") 
#[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")

$filestream = (Get-Content -Path $ConfigFile -Raw)
$script:NewHireConfigurationVariables = [PSCustomObject](ConvertFrom-Json -InputObject $filestream)

$global:Server = $script:NewHireConfigurationVariables.ad_server
Write-Output "$($MyInvocation.MyCommand)`t`tServer: $global:Server."
$global:DomainWriter = $script:NewHireConfigurationVariables.domain_credentials
Write-Output "$($MyInvocation.MyCommand)`t`tDomainWriter: $global:DomainWriter."
$global:OU = $script:NewHireConfigurationVariables.OU
Write-Output "$($MyInvocation.MyCommand)`t`tOU: $global:OU."


#region Servers
$tmp_servers = @{}
$global:Server = $global:Server.Split("|")
$global:Server  | ForEach-Object {
    $tmp = $_.split(":");
    $tmp_servers.Add($tmp[0],$tmp[1])
}
Clear-Variable Server
$global:Server = $tmp_servers
Clear-Variable tmp_servers
#endregion

#region Domain Credentials
$tmp_dw = @{}
$global:DomainWriter = $global:DomainWriter.Split("|")
$global:DomainWriter  | ForEach-Object {
    $tmp = $_.split(":");
    $tmp_dw.Add($tmp[0],$tmp[1])
}
Clear-Variable DomainWriter
$global:DomainWriter = $tmp_dw
Clear-Variable tmp_dw
#endregion

#region Organizational Units
<#This region should move to the module and become a Global #>
$tmp_ou = @{}
$global:OU = $global:OU.Split("|")
$global:OU  | ForEach-Object {
    $tmp = $_.split(":");
    $tmp_ou.Add($tmp[0],$tmp[1])
}
Clear-Variable OU
$global:OU = $tmp_ou
Clear-Variable tmp_ou
#endregion

$ValidateCompany = ($script:NewHireConfigurationVariables.company).Split(",")
$global:ValidCompany = @{}

foreach ($temp in $ValidateCompany)
{
    $tmp = $temp.Split(":")
    $global:ValidCompany[$tmp[0]] = $tmp[1]
}

Get-ChildItem -Path $PSSCriptRoot\Public  -Filter *-*.ps1 | ForEach-Object {. $_.FullName }
Get-ChildItem -Path $PSSCriptRoot\Private  -Filter *-*.ps1 | ForEach-Object {. $_.FullName }