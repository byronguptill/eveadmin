﻿$script:schema = @'
{
  "definitions": {},
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "http://example.com/root.json",
  "type": "object",
  "title": "The Root Schema",
  "required": [
      "company",
      "givenname",
      "surname",
      "title",
      "username",
      "department",
      "managername"
  ],
  "properties": {
    "company": {
      "$id": "#/properties/company",
      "type": "string",
      "title": "The Company Name Schema",
      "default": "",
      "examples": [
        "Sunrise Records"
      ],
      "pattern": "^(.*)$"
    },
    "givenname": {
      "$id": "#/properties/givenname",
      "type": "string",
      "title": "The Given Name Schema",
      "default": "",
      "examples": [
        "Pablo"
      ]
    },
    "surname": {
      "$id": "#/properties/surname",
      "type": "string",
      "title": "The Surname Name Schema",
      "default": "",
      "examples": [
        "Roque"
      ],
      "pattern": "^(.*)$"
    },
    "title": {
      "$id": "#/properties/title",
      "type": "string",
      "title": "The Title Schema",
      "default": "",
      "examples": [
        "Technology Manager"
      ],
      "pattern": "^(.*)$"
    },
    "username": {
      "$id": "#/properties/username",
      "type": "string",
      "title": "The Username Schema",
      "default": "",
      "examples": [
        "pablo"
      ],
      "pattern": "^(.*)$"
    },
    "department": {
      "$id": "#/properties/department",
      "type": "string",
      "title": "The Department Schema",
      "default": "",
      "examples": [
        "Technology Services"
      ],
      "pattern": "^(.*)$"
    },
    "managername": {
      "$id": "#/properties/managername",
      "type": "string",
      "title": "The Manager Name Schema",
      "default": "",
      "examples": [
        "Bob Putman"
      ],
      "pattern": "^(.*)$"
    }
  }
}
'@

function Read-NewHireJSON {
  [CmdletBinding()]
  param
  (
      [Parameter(Mandatory=$true)]
      [String]$Path
  )
  $JSONFiles = Get-ChildItem -Path $Path -Filter *.json 
  $Objects = New-Object System.Collections.ArrayList
  foreach ($file in $JSONFiles)
  {
    Write-Verbose "$($MyInvocation.MyCommand)`tGetting contents of $($file.Name)."
    if ( ([System.IO.FileInfo]($file.FullName)).Length -gt 0 )
    {
      $filestream = (Get-Content -Path $file.FullName -Raw)
      <# Powershell 6 +
      if (Test-json ($filestream) -Schema $script:schema )
      {
        #[void]$Objects.Add(@{$file.Name = [PSCustomObject](ConvertFrom-Json -InputObject $filestream -AsHashtable)})
        $Objects.Add([PSCustomObject]@{FileName = $file.Name; Object = [PSCustomObject](ConvertFrom-Json -InputObject $filestream -AsHashtable)}) | Out-Null
      }
      <# .net 4.7.2 #>
      [void]$Objects.Add([PSCustomObject]@{FileName = $file.Name; Object = [PSCustomObject](ConvertFrom-Json -InputObject $filestream)})
    }
  }
  , $Objects
}