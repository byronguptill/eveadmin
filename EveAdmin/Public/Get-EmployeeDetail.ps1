function Get-EmployeeDetail {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$false)]
        [ValidatePattern("\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b")]
        [String[]]
        $UserPrincipalName,

        [string]
        $Cr
    )
    
    begin {
        if (-not $PSBoundParameters.ContainsKey('Cr'))
        {
            If (Test-Path $ConfigFile)
            {
                $Cr = $script:NewHireConfigurationVariables.credentials
            }
        }
        if ($PSBoundParameters.ContainsKey('UserPrincipalName'))
        {
            $employees = Get-AzureADUserDetail -UserPrincipalName $UserPrincipalName -Cr $Cr
        }
        else
        {
            $employees = Get-AzureADUserDetail -Company EverestToys -Cr $ConfigFile
        }
        $FullDetail = New-object -TypeName System.Collections.ArrayList
    }
    
    process {
        foreach ($emp in $employees)
        {
            try {
            $ADUser = Get-ADUser -Identity $emp.MailNickName -Properties CanonicalName, CN, Company, Department, Description, DisplayName, DistinguishedName, EmailAddress, `
                GivenName, HomeDirectory, HomeDrive, `
                mail, Manager, MemberOf, Name, PrimaryGroup, `
                SamAccountName, ScriptPath, sn, Surname, `
                Title, UserPrincipalName, Enabled
            }
            catch
            {
                $ADUSer = [PSCustomObject]@{
                    SamaccountName = "Unable to resolve."
                }
            }

            if (get-adgroupmember -Identity SSLVPN-Users | Where-Object {$_.samaccountname -eq $($emp.MailNickName)})
            {
                $vpn = $true
            }
            else {
                $vpn = $false
            }
            $AllTheDeets = [PSCustomObject]@{
                ObjectId = $emp.ObjectId
                'FirstName' = $emp.FirstName
                'LastName' = $emp.LastName
                Title = $emp.Title
                'DisplayName' = $emp.DisplayName
                "MailNickName" = $emp.MailNickName
                "UserPrincipalName" = $emp.UserPrincipalName
                "Mail" = $emp.Mail
                Department = $emp.Department
                Enabled = $emp.AccountEnabled
                Intune = $emp.Intune
                E3 = $emp.E3
                Essentials = $emp.Essentials
                Manager = $emp.Manager   
                SamAccountName =  $ADUser.SamAccountName
                ADAccountEnabled = $ADUser.Enabled
                VPN = $vpn              
            }

            [void]$FullDetail.Add($AllTheDeets)
        }
    }
    
    end {
        ,$FullDetail
    }
}