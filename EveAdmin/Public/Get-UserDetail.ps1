﻿function Get-UserDetail {
    [CmdletBinding()]
    param (
        [ValidateNotNullOrEmpty()]
        [object]
        $Identity 
    )    
    process {
        $Identity | ForEach-Object `
        { 
            write-output "User Details for user object: $_";`
            get-aduser -Identity $_ -Properties CanonicalName, CN, Company, Department, Description, DisplayName, DistinguishedName, EmailAddress, `
            GivenName, HomeDirectory, HomeDrive, `
            mail, Manager, MemberOf, Name, PrimaryGroup, `
            SamAccountName, ScriptPath, sn, Surname, `
            Title, UserPrincipalName
        }
    }
}