<# #Requires -Modules Office365, ADDSEverestToysHelper #>
function New-EverestEmployee {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]$GivenName,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]$Surname,

        [Parameter(Mandatory=$false)]
        [ValidateNotNullOrEmpty()]
        [String]$Title,

        [Parameter(Mandatory=$false)]
        [ValidateNotNullOrEmpty()]
        [String]$Department,

        [Parameter(Mandatory=$true,
        HelpMessage="Enter the UPN of the Office 365 user account.  Typical format is username@domain.com")]
        [ValidateNotNullOrEmpty()]
        [String]$ManagerUPN,

        [ArgumentCompleter({ $global:ValidCompany.Keys })]
        [ValidateScript({$_ -in $Global:ValidCompany.Keys })]
        [String]$Company,

        [Parameter(Mandatory=$false)]
        [ValidateSet("Yes","No","")]
        [String]$Intune,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        $Respondant,
        
        [Parameter(Mandatory=$false)]
        $Username,

        [Parameter(Mandatory=$false)]
        $GroupMemberships,

        [Parameter(Mandatory=$false)]
        [ValidateSet("Laptop","Desktop","")]
        $ComputerRequired,

        [Parameter(Mandatory=$false)]
        [ValidateNotNullOrEmpty()]
        [ValidateSet("E3","Essentials","")]
        [String]$Office365Subscription,

        [Parameter(Mandatory=$true)]
        [ValidateSet("Yes","No")]
        $VPN,

        [Parameter(Mandatory=$false)]
        [ValidateNotNullOrEmpty()]
        $phonerequired,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        $Email,

        [Switch]
        $Test
    )

    begin {
        #region Global Settings
            # set variables from configuration of NewHire module (new_hire_config.json).
            $smtpServer = $script:NewHireConfigurationVariables.smtp_server
            Write-MyLog "$($MyInvocation.MyCommand)`t`tsmtpServer: $smtpServer."
            $FromAddress = $script:NewHireConfigurationVariables.from_address
            Write-MyLog "$($MyInvocation.MyCommand)`t`tFromAddress: $FromAddress."
            $BCC = $script:NewHireConfigurationVariables.bcc
            Write-MyLog "$($MyInvocation.MyCommand)`t`tBCC: $BCC."
            $SecurePasswordPolicy = $script:NewHireConfigurationVariables.secure_password_policy
            Write-MyLog "$($MyInvocation.MyCommand)`t`tSecurePasswordPolicy: $SecurePasswordPolicy."
            $CredentialFile = $script:NewHireConfigurationVariables.credentials
            Write-MyLog "$($MyInvocation.MyCommand)`t`tCredentialFile: $CredentialFile."
        #endregion

        $SCNonly = $script:NewHireConfigurationVariables.createscnaccountonly
        Write-MyLog "$($MyInvocation.MyCommand)`t`tSCNonly: $SCNonly."
        $VPNGroup = $script:NewHireConfigurationVariables.vpngroup
        Write-MyLog "$($MyInvocation.MyCommand)`t`tVPNGroup: $VPNGroup."
        
        #region Organizational Units
            <#This region should move to the module and become a Global #>
            $OU = $script:NewHireConfigurationVariables.OU
            Write-MyLog "$($MyInvocation.MyCommand)`t`tOU: $OU."

            $tmp_ou = @{}
            $OU = $OU.Split("|")
            $OU  | ForEach-Object {
                $tmp = $_.split(":");
                $tmp_ou.Add($tmp[0],$tmp[1])
            }
            Clear-Variable OU
            $OU = $tmp_ou
            Clear-Variable tmp_ou
        #endregion


        $SecurePassword = Get-SecureString
        $BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($SecurePassword)
        $images = @{ 
            image1 = ( Convert-TextToImage ([System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)) )
        }  
        Write-Output "$($MyInvocation.MyCommand)`t`images: $images."

        
        <#
            Clean up the setup mode area:
            1 - create SCN account
            2 - create on-prem AD account
            4 - create Azure AD account with and email license.
        #>


        switch ($Company)
        {
            "FYE"
            {
                if ($Department -match "Warehouse" -and $Title -match $SCNonly)
                {
                    $setupmode = 1
                }
                elseif ($Title -notmatch $SCNonly -and $email -eq "No") 
                {
                    $setupmode = 3
                }
            }
            default 
            {
                if ($Title -match $SCNonly -and $email -eq "No") {
                    $setupmode = 1
                }
                elseif ($Title -notmatch $SCNonly -and $email -eq "No") {
                    $setupmode = 2
                }
                elseif ($Department -match "Warehouse") {
                    $setupmode = 7
                }
                elseif ($email -eq "Only") {
                    $setupmode = 4      
                }
                elseif ($email -match "alexglobalproducts.com") {
                    $setupmode = -10      
                }
                else {
                    $setupmode = 6
                }
            }
        }

        Write-MyLog "$($MyInvocation.MyCommand)`t`Setup mode is configured to be: $setupmode."


        $MailNickName = New-Username -GivenName $GivenName -Surname $Surname $CredentialFile
        Write-MyLog "$($MyInvocation.MyCommand) Username: $MailNickName"

        $NewHireDsiplayName = Get-DisplayName -GivenName $GivenName -Surname $Surname -Title $Title
        Write-MyLog "$($MyInvocation.MyCommand) DisplayName: $NewHireDsiplayName"

        $SCNNickName = New-SCNUsername -GivenName $GivenName -Surname $Surname
        Write-MyLog "$($MyInvocation.MyCommand) SCNNickName: $SCNNickName"

        if ($setupmode -gt 1) {
            $AzureParams =  @{
                Company =$Company
                GivenName =$GivenName
                Surname =$Surname
                DisplayName =$NewHireDsiplayName
                Title =$Title
                Password = $SecurePassword
                MailNickName = $MailNickName
                Department = $Department
                ManagerUPN = $ManagerUPN
                Office365Subscription = $Office365Subscription
                Intune = $Intune
                Cr = $CredentialFile
                MFA = $true                
            }
            if ($PSBoundParameters.ContainsKey('GroupMemberships')) {
                $AzureParams.Add("GroupMemberships", $GroupMemberships)
            }
        }
    }

    process {
        <#
            If the setup mode is 4 or greater, we need to create an account in Azure AD with an email license.
        #>
        if ($setupmode -ge  4 ) 
        {
            <#
                Creates user account in AD and Azure AD.
            #>
            Write-MyLog "$($MyInvocation.MyCommand)`tLaunching [Office365]New-Employee."

            $AzureUserAccount = New-Employee @AzureParams
            Out-ClientAccessSetting -UserPrincipalName $AzureUserAccount.UserPrincipalName -FilePath $script:NewHireConfigurationVariables.message_folder_mailbox_configuration -Intune $Intune
        }

        <#
            create an AD user account
        #>
        if ( ($setupmode-2) -in (5,4,1,0)) {
            # create AD object - switch to company (what credentials to use)
            if ($setupmode -ne 4)
            {

                $UserObject = @{    
                    Company = $Company
                    GivenName = $GivenName
                    Surname = $Surname
                    Title = $Title
                    DisplayName = $NewHireDsiplayName 
                    Password = $SecurePassword
                    Department = $Department
                    ManagerMail = $ManagerUPN
                    MailNickName = $MailNickName
                    EmailAddress = $AzureUserAccount.UserPrincipalName
                    OU = $OU[$Company]
                }

                Write-MyLog "$($MyInvocation.MyCommand)`t`tAD parameters: `
                    Company = $Company `
                    GivenName = $GivenName `
                    Surname = $Surname `
                    Title = $Title `
                    DisplayName = $NewHireDsiplayName `
                    Password = $SecurePassword `
                    Department = $Department `
                    ManagerMail = $ManagerUPN `
                    MailNickName = $MailNickName `
                    EmailAddress = $AzureUserAccount.UserPrincipalName `
                    OU = $($OU[$Company])"

                switch ($Company) 
                {
                    "Alex Global Products" {
                        # get credentials for non implicit domain authentication.
                        $creds = $(Get-Content $global:DomainWriter[$Company]).Split("`n")
                        Write-Debug $creds[0]
                        Write-Debug $creds[1]
                        $local:UserCredential = New-Object System.Management.Automation.PSCredential -ArgumentList $creds[0], ($creds[1] | ConvertTo-SecureString)

                        # add credentials to authenticate against server.
                        $UserObject['Server'] = $global:Server[$Company]
                        $UserObject['Credential'] = $local:UserCredential

                        try {
                            # create AD user
                            New-ADDSUserObject @UserObject
                        }
                        catch {
                            Write-MyLog "An error occured calling New-ADDSUserObject"
                        }

                        # add to AD groups
                        $user = Find-ADDSUserBySamAccountName -samAccountName $AzureUserAccount.MailNickName -Server $global:Server[$Company] -Credential $local:UserCredential
                        if ($VPN -eq "Yes") {Add-ADDSGroupMember -Identity $VPNGroup -Members $user -Server $global:Server[$Company] -Credential $local:UserCredential}
                        break
                    }
                    "FYE" {
                        # add credentials to authenticate against server.
                        $UserObject['PasswordNeverExpires'] = $true

                        try {
                            # create AD user
                            New-ADDSUserObject @UserObject
                        }
                        catch {
                            Write-MyLog "An error occured calling New-ADDSUserObject"
                        }

                        break
                    }
                    default {
                        try {
                            # create AD user
                            New-ADDSUserObject @UserObject
                        }
                        catch {
                            Write-MyLog "An error occured calling New-ADDSUserObject"
                        }


                        # add to AD groups
                        $user = Get-ADDSUser -Identity $MailNickName
                        Add-ADDSGroupMember -Identity $SecurePasswordPolicy -Members $user
                        if ($VPN -eq "Yes") {Add-ADDSGroupMember -Identity $VPNGroup -Members $user}
                    }
                }
                Write-MyLog "$($MyInvocation.MyCommand) User created in on-prem AD: $user"
            }
        }

        <# 
            create a SCN account if user if a warehouse employee.
        #>
        if ( ($setupmode%2) -eq 1) {
            if ($email -eq "No")
            { 
                # no email required, this is just a scanning account.
                $DisplayName = "$NewHireDsiplayName SCN" 
                $TargetOU = $OU['SCN']
                $SamAccountName = $SCNNickName
            } else {
                $DisplayName = $NewHireDsiplayName
                $TargetOU = $OU[$Company]
                $SamAccountName = $MailNickName
            }

            New-ADDSUserObject  -Company $Company `
                                -GivenName $GivenName `
                                -Surname $Surname `
                                -Title $Title `
                                -DisplayName $DisplayName `
                                -Password $SecurePassword `
                                -Department $Department `
                                -ManagerMail $ManagerUPN `
                                -MailNickName $SamAccountName `
                                -PasswordNeverExpires $true `
                                -OU $TargetOU
        }

        #setup AD account in Alex Global Products for when an Office 365 account already exists.
        if ( $setupmode -eq -10 )
        {
            $creds = $(Get-Content $global:DomainWriter[$Company]).Split("`n")
            Write-Debug $creds[0]
            Write-Debug $creds[1]
            $local:UserCredential = New-Object System.Management.Automation.PSCredential -ArgumentList $creds[0], ($creds[1] | ConvertTo-SecureString)
            $AzureUserAccount = Get-AzureADUserDetail -UserPrincipalName $email

            ## need to add the credentials. ##
            New-ADDSUserObject  -Company $Company `
                                -GivenName $GivenName `
                                -Surname $Surname `
                                -Title $AzureUserAccount.Title `
                                -DisplayName $AzureUserAccount.DisplayName `
                                -Password $SecurePassword `
                                -Department $AzureUserAccount.Department `
                                -ManagerMail $AzureUserAccount.Manager `
                                -MailNickName $AzureUserAccount.MailNickName `
                                -EmailAddress $AzureUserAccount.UserPrincipalName `
                                -OU $OU[$Company] `
                                -Server $global:Server[$Company] `
                                -Credential $local:UserCredential

            $user = Find-ADDSUserBySamAccountName -samAccountName $AzureUserAccount.MailNickName -Server $global:Server[$Company] -Credential $local:UserCredential
            if ($VPN -eq "Yes") {Add-ADDSGroupMember -Identity $VPNGroup -Members $user -Server $global:Server[$Company] -Credential $local:UserCredential}
        }

        switch ($setupmode)
        {
            # Send message for SCN account
            {(($setupmode%2) -eq 1 ) -or ($setupmode -eq 2) } {
                if ($setupmode -eq 7) {$sm = 1} else {$sm =$setupmode}
                if ( $null -eq $DisplayName ) { $DisplayName = $NewHireDsiplayName }
                if ( $null -eq $SamAccountName ) { $SamAccountName = $MailNickName }
                $body = Get-HTMLMessage -Username $SamAccountName -ManagerName (Get-ADDSUser -Filter {mail -eq $ManagerUPN}).Name -NewHireDsiplayName $DisplayName -setupmode $sm
                $params = @{ 
                    InlineAttachments = $images 
                    Body = $body 
                    BodyAsHtml = $true 
                    Subject = "Your New Hire Setup: $DisplayName"
                    From = $FromAddress 
                    To = $ManagerUPN
                    Cc = $BCC
                    SmtpServer = $smtpServer 
                }
                if (-not ($Test.IsPresent) ){ Send-MailMessage @params }
                
            }
            3 
            {
                $body = Get-HTMLMessage -Username $MailNickName -ManagerName (Get-ADDSUser -Filter {mail -eq $ManagerUPN}).Name -NewHireDsiplayName $DisplayName -setupmode $sm
                $params = @{ 
                    InlineAttachments = $images 
                    Body = $body 
                    BodyAsHtml = $true 
                    Subject = "Your New Hire Setup: $DisplayName"
                    From = $FromAddress 
                    To = $ManagerUPN
                    Cc = $BCC
                    SmtpServer = $smtpServer 
                }
                if (-not ($Test.IsPresent) ){ Send-MailMessage @params }
            }
            # Send message for Az and AD account.
            {$_ -ge 4} {
                $body = Get-HTMLMessage -Username $MailNickName -ManagerName (Get-AzureADUserManager -ObjectId $AzureUserAccount.ObjectId).GivenName -NewHireDsiplayName $NewHireDsiplayName -NewHireEmailAddress $AzureUserAccount.UserPrincipalName -setupmode $setupmode
                $params = @{ 
                    InlineAttachments = $images 
                    Body = $body 
                    BodyAsHtml = $true 
                    Subject = "Your New Hire Setup: " + $NewHireDsiplayName
                    From = $FromAddress 
                    To = $ManagerUPN
                    Cc = $BCC
                    SmtpServer = $smtpServer 
                } 
                if (-not ($Test.IsPresent) ){ Send-MailMessage @params }
            }
        }
    }
    end {}
}
