function Set-MailboxAccessRules {
    [CmdletBinding()]
    param
    (
        [Parameter(Mandatory=$true)]
        [String]$Path
    )
    begin {
        Write-Verbose "Reading files from $Path"
        $JSONFiles = Get-ChildItem -Path $Path -Filter *.json 
        $Objects = New-Object System.Collections.ArrayList
        foreach ($file in $JSONFiles)
        {
            if ( ([System.IO.FileInfo]($file.FullName)).Length -gt 0 )
            {
                Write-Verbose "Processing $($file.FullName)"
                $filestream = (Get-Content -Path $file.FullName -Raw)
                <# Powershell 6 +
                if (Test-json ($filestream) -Schema $script:schema )
                {
                #[void]$Objects.Add(@{$file.Name = [PSCustomObject](ConvertFrom-Json -InputObject $filestream -AsHashtable)})
                $Objects.Add([PSCustomObject]@{FileName = $file.Name; Object = [PSCustomObject](ConvertFrom-Json -InputObject $filestream -AsHashtable)}) | Out-Null
                }
                <# .net 4.7.2 #>
                [void]$Objects.Add([PSCustomObject]@{FileName = $file.Name; Object = [PSCustomObject](ConvertFrom-Json -InputObject $filestream)})
            }
        }
    }
    process{
        foreach ($object in $Objects)
        {
            if ( [bool]($object.Object.PSobject.Properties.name -match "upn")  -and `
                 [bool]($object.Object.PSobject.Properties.name -match "AccessSetting"))
            {
                ## add some error handling.
                Write-Verbose "Found:  $($object.Filename)"
                Write-Verbose "Setting client access setting for $($object.Object.upn) to $($object.Object.AccessSetting)"
                Set-QuickCASMailbox -AccessSetting $object.Object.AccessSetting -UserPrincipalName $object.Object.upn $script:NewHireConfigurationVariables.credentials
                Write-Verbose "Rename $path\$($object.FileName) to $($object.FileName -replace 'json','processed')"
                Rename-Item -path "$path\$($object.FileName)" -NewName $($object.FileName -replace 'json','processed')
            }
        }
    }
    end {
        #need to tdelete files that are processed.
        , $Objects
    }
  }