﻿. $PSScriptRoot\New-ADDSUserObject.Config.ps1
function New-ADDSUserObject
{
    [CmdLetBinding(SupportsShouldProcess=$true, ConfirmImpact = 'low')]
    param
    (
            # Specify the domain the user will belong to.  I.E "everesttoys.com" or "sunriserecords.com"

            [Parameter(Mandatory=$true)]
            [ArgumentCompleter({ $global:ValidCompany.Keys })]
            [ValidateScript({$_ -in $Global:ValidCompany.Keys })]
            [string]$Company,
        
            # The first name of the user

            [Parameter(Mandatory=$true)]
            [String]$GivenName,

            # The family name of the user

            [Parameter(Mandatory=$true)]
            [String]$Surname,

            # The name that should appear in Global Address Llist and Outlook

            [Parameter(Mandatory=$false)]
            [String]$DisplayName,

            # The password for the user account

            [Parameter(Mandatory=$True)]
            [String]$Title,

            # The password for the user account

            [Parameter(Mandatory=$True)]
            [System.Security.SecureString]$Password,

            # The desired account name for the user  

            [Parameter(Mandatory=$false)]
            [String]$MailNickName,

            # The department the user will belong to

            [Parameter(Mandatory=$true)]
            [String]$Department,

            # The manager the user will be reporting to

            [Parameter(Mandatory=$true)]
            [String]$ManagerMail,

            [Parameter(Mandatory=$false)]
            [String]$OU = "OU=SBSUsers,OU=Users,OU=MyBusiness,DC=everest,DC=local",

            [Parameter(Mandatory=$false)]
            [String]$EmailAddress,

            [Parameter(Mandatory=$false)]
            [Boolean]
            $PasswordNeverExpires,

            [Parameter(Mandatory=$false)]
            [String]
            $Server,

            [Parameter(Mandatory=$false)]
            [PSCredential]
            $Credential
    )

    <#
    .SYNOPSIS
    Creates a User object in AD DS.

    .DESCRIPTION
    The user object that is created by this function will meet the minimum standard for a 
    user object that represents an employee of Everest Toys or Sunrise Records.

    .PARAMETER GivenName

    .PARAMETER Surname

    .PARAMETER MailNickName

    .EXAMPLE

    #>

    $ChangePasswordAtLogon = $true
    if ($PSBoundParameters.ContainsKey("Server") -and $PSBoundParameters.ContainsKey("Credential"))
    {
        $Manager = Get-ADUser -Filter {mail -eq $ManagerMail} -Server $Server -Credential $Credential
        Write-Verbose "$($MyInvocation.MyCommand) $manager"
    } else {
        $Manager = Get-ADUser -Filter {mail -eq $ManagerMail}
        Write-Verbose "$($MyInvocation.MyCommand) $manager"
    }

    #set username
    #pre 2000 username
    if ($PSCmdlet.ShouldProcess($DisplayName, "Create AD DS user object."))
    {
        try
        {
            switch ($PSBoundParameters.ContainsKey("Server") -and $PSBoundParameters.ContainsKey("Credential"))
            {
                $true {

                    New-ADUser -Company $Company `
                                -GivenName $GivenName `
                                -Surname $Surname `
                                -DisplayName $DisplayName `
                                -AccountPassword $Password `
                                -SamAccountName $MailNickName `
                                -Department $Department `
                                -Manager $Manager.DistinguishedName `
                                -UserPrincipalName $EmailAddress `
                                -HomeDrive $DefaultHomeDrive `
                                -HomeDirectory $DefaultHomeDirectory `
                                -Title $Title `
                                -Path $OU `
                                -EmailAddress $EmailAddress `
                                -ScriptPath $DefaultScriptPath `
                                -Name $DisplayName `
                                -ChangePasswordAtLogon $ChangePasswordAtLogon `
                                -Enabled $true `
                                -Server $Server `
                                -Credential $Credential
                }
                $false {
                    if ($DisplayName -match "SCN")
                    {
                        $ChangePasswordAtLogon = $false
                    }

                    New-ADUser -Company $Company `
                                -GivenName $GivenName `
                                -Surname $Surname `
                                -DisplayName $DisplayName `
                                -AccountPassword $Password `
                                -SamAccountName $MailNickName `
                                -Department $Department `
                                -Manager $Manager.DistinguishedName `
                                -UserPrincipalName (-join ($MailNickName, $DefaultDomain)) `
                                -HomeDrive $DefaultHomeDrive `
                                -HomeDirectory $DefailtHomeDirectory `
                                -Title $Title `
                                -Path $OU `
                                -EmailAddress $EmailAddress `
                                -ScriptPath $DefaultScriptPath `
                                -Name $DisplayName `
                                -ChangePasswordAtLogon $ChangePasswordAtLogon `
                                -Enabled $true
                }

            }
        }
        catch
        {
            
            $out = "$_ `
            -Company $Company `
            -GivenName $GivenName `
            -Surname $Surname `
            -DisplayName $DisplayName `
            -AccountPassword $Password `
            -SamAccountName $MailNickName `
            -Department $Department `
            -Manager $($Manager.DistinguishedName) `
            -UserPrincipalName (-join ($MailNickName, $DefaultDomain)) `
            -HomeDrive $DefaultHomeDrive `
            -HomeDirectory $DefailtHomeDirectory `
            -Title $Title `
            -Path $OU `
            -EmailAddress $EmailAddress `
            -ScriptPath $DefaultScriptPath `
            -Name `"$GivenName $Surname`"
            -ChangePasswordAtLogon $true `
            -Enabled $true" 
            Write-Error $out
        }
        $return
    }
}