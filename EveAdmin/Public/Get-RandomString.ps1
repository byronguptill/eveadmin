function Get-RandomString {
    -join ((33..126) | Get-Random -Count 16 | ForEach-Object {[char]$_})
}