
<#PSScriptInfo

.VERSION 1.0

.GUID 320e0be3-9784-41a3-a617-e9cf025d5246

.AUTHOR byron@everesttoys.com

.COMPANYNAME Everst Toys

.COPYRIGHT 

.TAGS 

.LICENSEURI 

.PROJECTURI 

.ICONURI 

.EXTERNALMODULEDEPENDENCIES Office365 eveadmin AzureAD ActiveDirectory

.REQUIREDSCRIPTS 

.EXTERNALSCRIPTDEPENDENCIES 

.RELEASENOTES


#>

<# 

.DESCRIPTION 
 Script to create new user object in AAD and AD DS. 

#> 
function Invoke-NewHire {
    [CmdletBinding()]
    Param(
        [ValidateNotNullorEmpty()]
        [string]
        $Path
    )

    begin {
        $Path = Resolve-Path -Path $Path
        Write-Output "$($MyInvocation.MyCommand)`tSearch $path for JSON files."
        Write-Output "$($MyInvocation.MyCommand)`t"
    }
    process {
        $NewHireObjects = Read-newhireJSON -path $path
        foreach ($NewHire in $NewHireObjects)
        {
            switch ($NewHire.object.emailonly)
            {
                "True" {
                    Write-Output "$($MyInvocation.MyCommand)`tEmail only."
                    $params = @{
                        GivenName = $NewHire.object.givenname
                        Surname = $NewHire.object.surname
                        Title = $NewHire.object.title
                        Email = "Only"
                        Department = $NewHire.object.department
                        ManagerUPN = $NewHire.object.managername
                        Company = $NewHire.object.company
                        Respondant = $NewHire.object.Respondant
                        Username = $NewHire.object.Username
                        GroupMemberships = $NewHire.object.GroupMemberships
                        Intune = $NewHire.object.Intune
                        VPN = "No"
                        phonerequired = "No"
                        Office365Subscription = "E3"
                        }
                    break
                }
                default {
                    Write-Output "$($MyInvocation.MyCommand)`tDefault."
                    $params = @{
                    GivenName = $NewHire.object.givenname
                    Surname = $NewHire.object.surname
                    Title = $NewHire.object.title
                    Email = $NewHire.object.email
                    Department = $NewHire.object.department
                    ManagerUPN = $NewHire.object.managername
                    Company = $NewHire.object.company
                    Respondant = $NewHire.object.Respondant
                    Username = $NewHire.object.Username
                    GroupMemberships = $NewHire.object.GroupMemberships
                    ComputerRequired = $NewHire.object.ComputerRequired
                    Intune = $NewHire.object.Intune
                    VPN = $NewHire.object.VPN
                    phonerequired=$NewHire.object.phonerequired
                    Office365Subscription = "E3"
                    }
                }
            }

            try 
            {
                Write-Output "$($MyInvocation.MyCommand)`t$(Get-Date -Format "MM/dd/yyyy HH:mm K") Launching the New-EverestEmployee function."
                New-EverestEmployee @params 
                Rename-Item -path "$path\$($NewHire.FileName)" -NewName $($NewHire.FileName -replace 'json','processed')
            } 
            catch 
            {
                Write-Error "$PSItem"
            }
        }

    }
}