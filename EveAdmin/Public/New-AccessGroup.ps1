﻿function New-AccessGroup {
    [CmdletBinding(SupportsShouldProcess, ConfirmImpact = 'Low')]
    param 
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]$sharename
    )
    begin {
        $prefix = "Access"
        $ro = "$prefix-RO-$sharename"
        $rw = "$prefix-RW-$sharename"
        $names = @($ro, $rw)
        $path = "OU=Access Groups,OU=Security Groups,OU=Groups,OU=MyBusiness,DC=everest,DC=local"
    }
    process {
        if ($PSCmdlet.ShouldProcess($sharename, "Create new access groups in Active Directory.`n`t$ro`n`t$rw"))
        {
            $names | ForEach-Object {New-ADGroup -Name $_ -SamAccountName $_ -GroupCategory Security -Path $path -GroupScope Universal -PassThru }    
        }
    }
}
